import java.util.Arrays;
public class Task5 {
    public static void main(String[] args) {
        int[] arr = new int[] {1,5,8,7,123,478,118,-15,0,-123,-34};

        int preMaxElement = getPreMaxElement(arr);

        resultPrint(preMaxElement);
    }

    public static int getPreMaxElement(int[] arr) {
    Arrays.sort(arr);
    int ind = arr.length - 2;
    return arr[ind];
    }

    public static void resultPrint(int preMaxElement){
        System.out.println("Second max element is " +  preMaxElement);
    }
}
