import java.util.Scanner;
import java.util.Arrays;

public class Task2 {
    public static void main(String[] args) {
        int a, b, c, d, GCD;

        a = inputIntFromConsole("Enter the value of A: ");
        b = inputIntFromConsole("Enter the value of B: ");
        c = inputIntFromConsole("Enter the value of C: ");
        d = inputIntFromConsole("Enter the value of D: ");

        GCD = findFourNumbersGCD(a,b,c,d); // NOD

        resultPrint(GCD);
    }
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Invalid input " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int findGCD(int a, int b) {
        int GCD;
        while (a != b) {         //Euclid's algorithm
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        GCD = a;
        return GCD;
    }

    public static int findFourNumbersGCD(int a, int b, int c, int d) {
        int arrayGCDs[] = new int[6];
        int value;
        arrayGCDs[0] = findGCD(a, b);
        arrayGCDs[1] = findGCD(b, c);
        arrayGCDs[2] = findGCD(c, d);
        arrayGCDs[3] = findGCD(a, d);
        arrayGCDs[4] = findGCD(b, d);
        arrayGCDs[5] = findGCD(a, c);


        Arrays.sort(arrayGCDs);
        if(arrayGCDs[5]%arrayGCDs[0]!=0 || arrayGCDs[0]==1){return 1;}
        return arrayGCDs[0];
    }


    public static void resultPrint(int GCD) {
        System.out.printf("The greatest common divisor(GCD) of A, B, C and D equals " + GCD + "." );
    }
}
