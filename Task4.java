import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task4 {
    public static void main(String[] args) {
        int[] arrFactorial = getOddNumbers();


        int sum = getSumOfFactorialsOfOddNums(arrFactorial);
        resultPrint(sum);
    }

    public static int[] getOddNumbers() {
        int arr[] = new int[5];
        for (int i = 1; i <= 9; i+=2) {
            arr[(i-1)/2] = i;
        }
        return arr;
    }
    public static int getFactorial(int n) {
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }
    public static int getSumOfFactorialsOfOddNums(int[] arr){
        int sum = 0;
        int fact;

        for(int i = 0; i < arr.length; i++){
                fact = getFactorial(arr[i]);
                sum += fact;
        }
        return sum;
    }
    public static void resultPrint(int sum){
        System.out.println("Sum of factorials of odd numbers between 1 and 9 is " + sum + ".");
    }
}
